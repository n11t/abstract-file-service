<?php
declare(strict_types=1);

namespace N11t\AbstractFileService;

use N11t\AbstractFileService\Exception\JsonParseException;

abstract class AbstractFileService
{

    /**
     * @var string
     */
    private $filePath;

    public function __construct(string $directory)
    {
        $this->validateDirectory($directory);

        $file = $directory . '/' . $this->getFileName();

        if (!is_file($file)) {
            touch($file);
        }

        $this->filePath = $file;
    }

    /**
     * Get the filename for current file. This method will be called once in the constructor.
     * So be sure, that you can return the filename correctly before calling parent::__construct.
     *
     * @return string
     */
    abstract protected function getFileName(): string;

    protected function validateDirectory(string $directory): void
    {
        if (!is_dir($directory)) {
            throw new \ InvalidArgumentException(sprintf('Directory %s not found.', $directory));
        }
    }

    protected function getFilePath(): string
    {
        return $this->filePath;
    }

    protected function read(): string
    {
        return file_get_contents($this->getFilePath());
    }

    protected function write(string $content): void
    {
        file_put_contents($this->getFilePath(), $content);
    }

    protected function writeJson(array $array): void
    {
        $content = json_encode($array);

        $this->write($content);
    }

    /**
     * Decode file content as json.
     *
     * @return array
     * @throws JsonParseException
     */
    protected function readJson(): array
    {
        $content = $this->read();

        $data = json_decode($content, $assoc = true);

        if (!\is_array($data)) {
            throw new JsonParseException();
        }

        return $data;
    }
}
