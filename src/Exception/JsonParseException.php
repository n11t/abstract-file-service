<?php
declare(strict_types=1);

namespace N11t\AbstractFileService\Exception;

class JsonParseException extends \Exception
{

    public function __construct()
    {
        parent::__construct('Unable to parse file content.');
    }
}
