[![pipeline status](https://gitlab.com/n11t/abstract-file-service/badges/master/pipeline.svg)](https://gitlab.com/n11t/abstract-file-service/commits/master) [![coverage report](https://gitlab.com/n11t/abstract-file-service/badges/master/coverage.svg)](https://gitlab.com/n11t/abstract-file-service/commits/master)

# AbstractFileService

This class can be used to create file based services.
