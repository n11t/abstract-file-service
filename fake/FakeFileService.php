<?php
declare(strict_types=1);

namespace N11t\Fake\AbstractFileService;

use N11t\AbstractFileService\AbstractFileService;

class FakeFileService extends AbstractFileService
{

    /**
     * @var string
     */
    private $filename;

    public function __construct(string $directory, string $filename)
    {
        $this->filename = $filename;

        parent::__construct($directory);
    }

    public function readFile(): string
    {
        return $this->read();
    }

    public function writeFile(string $content): void
    {
        $this->write($content);
    }

    public function writeJsonFile(array $array): void
    {
        $this->writeJson($array);
    }

    public function readJsonFile(): array
    {
        return $this->readJson();
    }

    protected function getFileName(): string
    {
        return $this->filename;
    }
}
