<?php
declare(strict_types=1);

namespace N11t\AbstractFileService;

use N11t\Fake\AbstractFileService\FakeFileService;
use N11t\PHPUnit\DirectoryTestCase;

class AbstractFileServiceTest extends DirectoryTestCase
{

    /**
     * @var string
     */
    private $directory;

    public function getAssetsDirectory(): string
    {
        return __DIR__ . '/Assets';
    }

    protected function setUp()
    {
        parent::setUp();

        $this->directory = $this->createDirectory('/fakeFileService');
    }

    public function testCanCreateFileIfNotExists()
    {
        // Act
        new FakeFileService($this->directory, 'fileToCreate.json');

        // Assert
        $expectedFile = $this->directory . '/fileToCreate.json';

        self::assertFileExists($expectedFile);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /Directory .*\/fakeFileServiceDoesNotExist not found./
     */
    public function testWillThrowExceptionIfDirectoryDoesNotExists()
    {
        // Arrange
        $directory = $this->directory . 'DoesNotExist';

        // Act
        new FakeFileService($directory, 'file.json');
    }

    public function testCanReadFileIfAlreadyExists()
    {
        // Arrange
        $path = '/fakeFileService2';

        $directory = $this->createDirectory($path);
        $file = $this->copyAsset('file.json', $path . '/file.json');

        // Act
        $service = new FakeFileService($directory, 'file.json');
        $content = $service->readFile();

        // Assert
        self::assertStringEqualsFile($file, $content);
    }

    public function testCanWriteFile()
    {
        // Arrange
        $content = 'Hello World, how are you?';

        // Act
        $service = new FakeFileService($this->directory, 'file.json');
        $service->writeFile($content);

        // Assert
        self::assertStringEqualsFile($this->directory . '/file.json', $content);
    }

    public function testCanWriteJson()
    {
        // Arrange
        $content = [
            'Hallo' => 'Welt',
            1 => [
                'How' => 'Are',
                'You' => 'Today',
                '?'
            ]
        ];

        // Act
        $service = new FakeFileService($this->directory, 'file.json');
        $service->writeJsonFile($content);

        // Assert
        $expectedContent = '{"Hallo":"Welt","1":{"How":"Are","You":"Today","0":"?"}}';
        self::assertStringEqualsFile($this->directory . '/file.json', $expectedContent);
    }

    public function testCanReadJson()
    {
        // Arrange
        $this->copyAsset('complex.json', 'fakeFileService/file.json');

        // Act
        $service = new FakeFileService($this->directory, 'file.json');
        $actualContent = $service->readJsonFile();

        // Arrange
        $expectedContent = [
            'Hallo' => 'Welt',
            1 => [
                'How' => 'Are',
                'You' => 'Today',
                '?'
            ]
        ];
        self::assertSame($expectedContent, $actualContent);
    }

    /**
     * @expectedException \N11t\AbstractFileService\Exception\JsonParseException
     */
    public function testWillThrowExceptionIfJsonCanNotBeParsed()
    {
        // Act
        $service = new FakeFileService($this->directory, 'file.json');
        $service->readJsonFile();
    }
}
